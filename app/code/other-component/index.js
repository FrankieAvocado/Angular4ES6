/**
 * Created by ebates007 on 7/17/2017.
 */
import {Component} from '@angular/core';

var OtherComponent = Component({
    selector: "other-component",
    template: "<h1>I'm an alternative!</h1>"
}).Class({
    constructor: function(){}
});

export{OtherComponent};