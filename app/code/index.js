import 'reflect-metadata';
import 'zone.js';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app-component/index.js";
import { HomeComponent } from "./home-component/index.js";
import { OtherComponent } from "./other-component/index.js"
import { BrowserModule } from "@angular/platform-browser";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

var appRoutes = [
    {
        path: "home", component: HomeComponent
    },
    {
        path: "other", component: OtherComponent
    }
];


var appModule = NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes)
    ],
    declarations: [AppComponent, HomeComponent, OtherComponent],
    bootstrap: [AppComponent]
}).Class({
   constructor: function(){}
});

document.addEventListener("DOMContentLoaded", () => {
   platformBrowserDynamic().bootstrapModule(appModule);
});