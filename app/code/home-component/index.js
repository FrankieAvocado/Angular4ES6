import {Component} from '@angular/core';

var HomeComponent = Component({
    selector: "home-component",
    template: "<h1>Hello World!</h1>"
}).Class({
    constructor: function(){}
});

export{HomeComponent};