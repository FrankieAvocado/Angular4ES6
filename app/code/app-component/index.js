/**
 * Created by ebates007 on 7/17/2017.
 */
import {Component} from '@angular/core';

var AppComponent = Component({
    selector: "app-component",
    template: "<h1>Currently Running Router?</h1><router-outlet></router-outlet>"
}).Class({
    constructor: function(){}
});

export{AppComponent};