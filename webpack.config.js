var webpack = require("webpack");
var path = require("path");

var config = {
	entry:  path.join(__dirname, "./app/code/index.js"),
	output: {
		path: path.join(__dirname, "./app/site/dist"),
		filename: "bundle.js"
	},
	module: {
		 loaders: [
            {
                test : /\.(js)?/,
                loader : "babel-loader",
                query: {
                    presets: ["es2015"]
                }
            }
		]
	}
};

module.exports = config;