var express = require("express");
var path = require("path");
var app = express();
var port = 3000;

app.use(express.static( __dirname + "/app/site"));
app.use("*",function(req,res){
    res.sendFile(path.join(__dirname,"app/site/index.html"));
});

app.listen(port);

console.log("The audience is listening on port " + port + ". . . ");